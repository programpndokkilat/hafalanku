package com.blank.root.hafalanalquran;

import android.content.Context;

/**
 * Created by root on 10/08/16.
 */
public interface Activity {
    interface ProfilActivity{
        void profil();
    }
    void errorLogin();
    void dataKosong();
    void successLogin();
}
