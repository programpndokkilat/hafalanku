package com.blank.root.hafalanalquran.Profil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.blank.root.hafalanalquran.Activity;
import com.blank.root.hafalanalquran.R;

public class Profil extends AppCompatActivity implements Activity.ProfilActivity{



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_profil);
        profil();
    }

    @Override
    public void profil() {
        TextView textView = new TextView(this);
        textView.setText("Welcome Santri");
        textView.setWidth(100);
        textView.setHeight(500);
        setContentView(textView);
    }
}
